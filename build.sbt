import play.Project._

name := "expense-tracker"

version := "1.0"

playScalaSettings

routesImport += "org.expenses.application.Global._"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.scalatestplus" % "play_2.10" % "1.0.0" % "test",
  "org.mockito" % "mockito-all" % "1.9.5" % "test"
)