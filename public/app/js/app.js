'use strict';


// Declare app level module which depends on filters, and services
angular.module('expenses', [
  'ngRoute',
  'expenses.filters',
  'expenses.services',
  'expenses.directives',
  'expenses.controllers',
  'ngGrid',
  'ui.bootstrap',
  'ngCookies'
]).
config(['$routeProvider', function($routeProvider) {
    $routeProvider


        .when('/expenses', {
            templateUrl : 'pages/expenses.html',
            controller  : 'expensesController'
        })

        .when('/expenses/new', {
            templateUrl : 'pages/edit-expense.html',
            controller  : 'expenseCreateController'
        })

        .when('/expenses/:id', {
            templateUrl : 'pages/edit-expense.html',
            controller  : 'expenseEditController'
        })

        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })

        .when('/register', {
            templateUrl : 'pages/register.html',
            controller  : 'registerController'
        })

        .when('/report', {
            templateUrl : 'pages/week-report.html',
            controller  : 'reportController'
        })

        .when('/logout', {
            templateUrl : 'pages/login.html',
            controller  : 'logoutController'
        })

        .otherwise({redirectTo: '/expenses'});

}]);
