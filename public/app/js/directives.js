'use strict';

/* Directives */


angular.module('expenses.directives', []).
    directive('numberOnlyInput', function () {
        return {
            restrict: 'EA',
            template: '<input name="{{inputName}}" ng-model="inputValue" class="form-control" required/>',
            scope: {
                inputValue: '=',
                inputName: '='
            },
            link: function (scope) {
                scope.$watch('inputValue', function(newValue, oldValue) {
                    var arr = String(newValue).split("");
                    if (arr.length === 0) return;
                    if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                    if (arr.length === 2 && newValue === '-.') return;
                    if (isNaN(newValue)) {
                        scope.inputValue = oldValue;
                    }
                });
            }
        };
    }).
    directive('userControl', function ($rootScope, userService) {
        return {
            restrict: 'EA',
            template: '<ul class="nav navbar-nav navbar-right">' +
                            '<li><a href="#register" ng-hide="currentUser != null">Register</a></li>' +
                            '<li><a href="#login" ng-hide="currentUser != null">Login</a></li>' +
                            '<li class="dropdown" ng-show="currentUser != null">' +
                                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{currentUser.name}}<b class="caret"></b></a>' +
                                '<ul class="dropdown-menu"><li><a href="#logout">Logout</a></li></ul>' +
                            '</li>' +
                      '</ul>',
            link: function () {

                userService.currentUser().success(function (data) {
                    $rootScope.currentUser = data;
                }).error(function(data, status) {
                    if (status == 401) {
                        $rootScope.currentUser = null;
                    }
                });

            }
        };
    }).
    directive('weekPicker', function (expenseService) {
        return {
            restrict: 'EA',
            template: '<div class="week-picker"></div>',
            scope: {
                startDate: '=',
                endDate: '='
            },
            link: function (scope, element, attrs) {

                var selectCurrentWeek = function() {
                    window.setTimeout(function () {
                        element.find('.ui-datepicker-current-day a').addClass('ui-state-active')
                    }, 1);
                };

                var now = new Date();
                scope.startDate = expenseService.shiftDateBack(now);
                scope.endDate = expenseService.shiftDateForward(now);
                selectCurrentWeek();

                $('.week-picker').datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        firstDay: 1,
                        onSelect: function(dateText, inst) {
                            var date = $(this).datepicker('getDate');
                            scope.startDate = expenseService.shiftDateBack(date);
                            scope.endDate = expenseService.shiftDateForward(date);
                            scope.$apply();

                            selectCurrentWeek();
                        },
                        beforeShowDay: function(date) {
                            var cssClass = '';
                            if(date >= scope.startDate && date <= scope.endDate)
                                cssClass = 'ui-datepicker-current-day';
                            return [true, cssClass];
                        },
                        onChangeMonthYear: function(year, month, inst) {
                            selectCurrentWeek();
                        }
                    });

                $('.week-picker').on('mousemove', '.ui-datepicker-calendar tr', function() { $(this).find('td a').addClass('ui-state-hover'); });
                $('.week-picker').on('mouseleave', '.ui-datepicker-calendar tr', function() { $(this).find('td a').removeClass('ui-state-hover'); });

            }
        }
    });
