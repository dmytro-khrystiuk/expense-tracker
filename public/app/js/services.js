'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('expenses.services', []).
    factory('expenseService', function($http, $filter) {

        var expenseAPI = {};

        var shiftDateFromWeekStartBy = function (date, shift) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1 + shift);
        };

        expenseAPI.shiftDateBack = function(date) {
            return shiftDateFromWeekStartBy(date, 0);
        };

        expenseAPI.shiftDateForward = function(date) {
            return shiftDateFromWeekStartBy(date, 6);
        };

        expenseAPI.getExpenses = function(sortOptions, filterData) {
            var sortField = sortOptions.directions[0] === "desc" ? "-" : "+";
            sortField += sortOptions.fields[0];

            var parameters = filterData;
            parameters['orderBy'] = sortField;

            return $http({
                method: 'GET',
                url: '/expenses',
                params: parameters
            });
        };

        expenseAPI.getExpense = function(id) {
            return $http({
                method: 'GET',
                url: '/expenses/' + id
            });
        };

        expenseAPI.updateExpense = function(expense) {
            return $http({
                method: 'POST',
                url: '/expenses/update',
                data: expense
            });
        };

        expenseAPI.createExpense = function(expense) {
            return $http({
                method: 'PUT',
                url: '/expenses/new',
                data: expense
            });
        };

        expenseAPI.reportForRange = function(startDate, endDate) {
            var format = 'yyyy-MM-dd';
            var shiftedEndDate = shiftDateFromWeekStartBy(startDate, 7);
            return $http({
                method: 'GET',
                url: '/expenses/report/' + $filter('date')(startDate, format) + '/' + $filter('date')(shiftedEndDate, format)
            });
        };

        return expenseAPI;
    }).
    factory('userService', function($http, $location, $rootScope) {

        var userAPI = {};

        userAPI.currentUser = function() {
            return $http({
                method: 'GET',
                url: '/user/current'
            });
        };

        userAPI.authenticate = function(user) {
            return $http({
                method: 'POST',
                url: '/authenticate',
                data: user
            });
        };

        userAPI.register = function(user) {
            return $http({
                method: 'PUT',
                url: '/register',
                data: user
            });
        };

        userAPI.logout = function() {
            return $http({
                method: 'GET',
                url: '/logout'
            });
        };

        userAPI.reactToError = function (scope, data, status) {
            if (status == 401) {
                $rootScope.currentUser = null;
                $location.path('/login');
            } else {
                scope.hasErrors = true;
                scope.errorMessage = data.error;
            }
        };

        return userAPI;
    });
