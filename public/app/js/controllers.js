'use strict';

/* Controllers */

angular.module('expenses.controllers', [])
    .controller('expensesController', function ($scope, $filter, expenseService, userService) {
        $scope.expenseList = [];
        $scope.filterData = [];

        $scope.expenseFilterDate = "";
        $scope.expenseFilterDescription = "";
        $scope.expenseFilterAmount = "";

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };


        $scope.format = 'yyyy-MM-dd';
        $scope.sortOptions = {
            fields: ["creationDate"],
            directions: ["desc"]
        };

        $scope.gridOptions = {
            data: 'expenseList',
            multiSelect: false,
            enableRowSelection: false,
            useExternalSorting: true,
            sortInfo: $scope.sortOptions,
            columnDefs: [
            {field: 'creationDate', displayName: 'Date Created'},
            {field: 'description', displayName: 'Description'},
            {field: 'amount', displayName: 'Amount'},
            {field: 'id', displayName: '', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text><a href="#/expenses/{{row.getProperty(col.field)}}">Edit</a></span></div>'}
        ]};

        $scope.refresh = function() {
            setTimeout(function () {
                expenseService.getExpenses($scope.sortOptions, $scope.filterData).success(function (response) {
                    $scope.expenseList = response;
                }).error(function(data, status) {
                    userService.reactToError($scope, data, status);
                });
            }, 100);
        };

        $scope.$watch('sortOptions', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.refresh();
            }
        }, true);

        $scope.$watch('expenseFilterDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.filterData['creationDate'] = $filter('date')(newVal, $scope.format);
                $scope.refresh();
            }
        }, true);

        $scope.$watch('expenseFilterDescription', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.filterData['description'] = newVal;
                $scope.refresh();
            }
        }, true);

        $scope.$watch('expenseFilterAmount', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.filterData['amount'] = newVal;
                $scope.refresh();
            }
        }, true);

        $scope.refresh();
    })
    .controller('expenseEditController', function ($scope, $routeParams, $location, expenseService, userService) {
        $scope.formLabel = 'Edit Expense';
        $scope.format = 'yyyy-MM-dd';
        $scope.expense = null;

        expenseService.getExpense($routeParams.id).success(function(response) {
            $scope.expense = response;

            $scope.formData = {
                creationDate: new Date($scope.expense.creationDate),
                creationTime: $scope.expense.creationDate,
                description: $scope.expense.description,
                amount: $scope.expense.amount,
                comment: $scope.expense.comment
            };
        }).error(function (data, status) {
            userService.reactToError($scope, data, status);
        });

        $scope.processForm = function() {
            var date = $scope.formData.creationDate;
            var time = new Date($scope.formData.creationTime);
            $scope.expense.creationDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes());
            $scope.expense.description = $scope.formData.description;
            $scope.expense.amount = $scope.formData.amount;
            $scope.expense.comment = $scope.formData.comment;

            expenseService.updateExpense($scope.expense).success(function() {
                $location.path('/expenses');
            }).error(function(data, status) {
                userService.reactToError($scope, data, status);
            });
        };

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

    })
    .controller('expenseCreateController', function ($scope, $location, $rootScope, expenseService, userService) {
        $scope.formLabel = 'Create Expense';
        $scope.format = 'yyyy-MM-dd';
        $scope.expense = {};
        $scope.formData = {
            creationDate: new Date(),
            creationTime: new Date(),
            amount: 0
        };

        $scope.processForm = function() {
            var date = $scope.formData.creationDate;
            var time = new Date($scope.formData.creationTime);
            $scope.expense.creationDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes());
            $scope.expense.description = $scope.formData.description;
            $scope.expense.amount = $scope.formData.amount;
            $scope.expense.comment = $scope.formData.comment;
            $scope.expense.userId = $rootScope.currentUser.id;

            expenseService.createExpense($scope.expense).success(function() {
                $location.path('/expenses');
            }).error(function(data, status) {
                userService.reactToError($scope, data, status);
            });
        };

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

    })
    .controller('loginController', function($scope, $location, $rootScope, userService) {
        $scope.formData = {
            login: '',
            password: ''
        };

        $scope.processForm = function() {
            if ($scope.formData.login == '' || $scope.formData.password == '') {
                $scope.missingFields = true;
            } else {
                userService.authenticate($scope.formData).success(function(data) {
                    $rootScope.currentUser = data;
                    $location.path('/expenses');
                }).error(function(data, status) {
                    $scope.hasErrors = true;
                    $scope.errorMessage = (status == 401) ? 'Incorrect username or password' : data.error;
                });
            }
        };

    })
    .controller('logoutController', function($location, userService, $rootScope) {
        userService.logout().success(function () {
            $rootScope.currentUser = null;
            $location.path('/login');
        }).error(function(data, status) {
            userService.reactToError($scope, data, status);
        });
    })
    .controller('registerController', function($scope, $location, $rootScope, userService) {
        $scope.formData = {
            login: '',
            name: '',
            password: '',
            passwordConfirmation: ''
        };

        $scope.processForm = function() {
            if ($scope.formData.password != '' && $scope.formData.password != $scope.formData.passwordConfirmation) {
                $scope.hasErrors = true;
                $scope.errorMessage = 'Entered passwords does not match';
                $scope.formData.password = '';
                $scope.formData.passwordConfirmation = '';
                $scope.registerForm.password.$pristine = true;
                $scope.registerForm.passwordConfirmation.$pristine = true;
            } else {
                var user = {
                    login: $scope.formData.login,
                    name: $scope.formData.name,
                    password: $scope.formData.password
                };
                userService.register(user).success(function (data) {
                    $rootScope.currentUser = data;
                    $location.path('/expenses');
                }).error(function (data, status) {
                    userService.reactToError($scope, data, status);
                });
            }

        };

    })
    .controller('reportController', function($scope, $location, $rootScope, userService, expenseService) {
        $scope.report = {};

        $scope.gridOptions = {
            data: 'report.expenses',
            multiSelect: false,
            enableRowSelection: false,
            useExternalSorting: true,
            enableSorting: false,
            columnDefs: [
                {field: 'creationDate', displayName: 'Date Created'},
                {field: 'description', displayName: 'Description'},
                {field: 'amount', displayName: 'Amount'}
            ]};

        $scope.refresh = function () {
            expenseService.reportForRange($scope.startDate, $scope.endDate).success(function (data) {
                $scope.report = data;
            }).error(function (data, status) {
                userService.reactToError($scope, data, status);
            });
        };

        $scope.$watch('startDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.refresh();
            }
        }, true);
        $scope.$watch('endDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.refresh();
            }
        }, true);

    });
