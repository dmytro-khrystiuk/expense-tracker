package org.expenses.models

import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._

case class User(id: Option[Long], login: String, name: String, password: String)

object User {

  object Field {
    val Id = "id"
    val Login = "login"
    val Name = "name"
    val Password = "password"
  }

  implicit object UserFormat extends Format[User] {

    override def reads(json: JsValue): JsResult[User] = (
      (JsPath \ Field.Id).readNullable[Long](min(1L)) and
      (JsPath \ Field.Login).read[String](minLength[String](1)) and
      (JsPath \ Field.Name).read[String](minLength[String](1)) and
      (JsPath \ Field.Password).read[String](minLength[String](1))
    )(User.apply _).reads(json)

    override def writes(user: User): JsValue = Json.obj(
      Field.Id -> user.id,
      Field.Login -> user.login,
      Field.Name -> user.name
    )

  }
}
