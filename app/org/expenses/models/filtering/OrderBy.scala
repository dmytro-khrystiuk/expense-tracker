package org.expenses.models.filtering

import org.expenses.application.Global

case class OrderBy(direction: String, field: String)

object OrderBy {
  val Asc = "ASC"
  val Desc = "DESC"

  def apply(input: String): OrderBy = {
    val pattern = """(\+|-)?(\w+)""".r
    val pattern(direction, field) = input
    val underscoredField = Global.camelToUnderscores(field)

    direction match {
      case "+" | null => OrderBy(Asc, underscoredField)
      case "-" => OrderBy(Desc, underscoredField)
    }
  }

}
