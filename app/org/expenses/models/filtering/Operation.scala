package org.expenses.models.filtering

object Operation extends Enumeration {
  type Operation = Value
  val EQUAL = Value("=")
  val LIKE = Value("LIKE")
  val GREATER_THAN = Value(">")
  val LESS_THAN = Value("<")
  val GREATER_THAN_OR_EQUAL = Value(">=")
  val LESS_THAN_OR_EQUAL = Value("<=")
}
