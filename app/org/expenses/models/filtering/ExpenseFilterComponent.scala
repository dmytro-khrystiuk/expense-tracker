package org.expenses.models.filtering

import org.expenses.models.filtering.Operation._
import org.expenses.application.Global
import scala.util.Try

trait ExpenseFilterComponent {
  def newExpenseFilter: ExpenseFilter
  val expenseFilterHelper: ExpenseFilterHelper

  type Extractor = String => Option[Any]
  
  class ExpenseFilter(val criteria: Set[Criterion]) {

    def withCriterion(field: String, input: String) = if (!input.isEmpty) {
      val adoptedField = Global.camelToUnderscores(field)
      val (receivedOperation, value) = getOperationAndValue(field, input)
      expenseFilterHelper.defaultOps.get(adoptedField).flatMap { case (_, extractor) =>
        extractor(value) }.fold(this) {
        extracted =>
          val operation = receivedOperation.getOrElse(expenseFilterHelper.defaultOps(adoptedField)._1)
          new ExpenseFilter(criteria + Criterion(adoptedField, operation, extracted))
      }
    } else this

    def withCriterion(field: String, operation: Operation, value: Any): ExpenseFilter = {
      expenseFilterHelper.defaultOps.keySet.find(_ == Global.camelToUnderscores(field)) match {
        case Some(fieldName) => new ExpenseFilter(criteria + Criterion(fieldName, operation, value))
        case None => this
      }
    }

    private def getOperationAndValue(field: String, input: String): (Option[Operation], String) = {
      def getOperation(operationString: String) = Try(Operation.withName(operationString)).toOption

      input.split(" ") match {
        case Array(value) => (None, value)
        case Array(opString, value) => (getOperation(opString), value)
        case Array(opString, tail @_*) => (getOperation(opString), tail mkString " ")
      }
    }

  }
  
  trait ExpenseFilterHelper {
    val defaultOps: Map[String, (Operation, Extractor)]
    lazy val FilterFields = defaultOps.keySet.map(Global.underscoreToCamel)
  }

}
