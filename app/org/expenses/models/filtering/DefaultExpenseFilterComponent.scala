package org.expenses.models.filtering

import org.expenses.models.filtering.Operation._
import scala.Some
import org.expenses.models.Expense.Field
import scala.util.Try

trait DefaultExpenseFilterComponent extends ExpenseFilterComponent {
  override def newExpenseFilter = new ExpenseFilter(Set.empty)
  override val expenseFilterHelper = DefaultFilterHelper

  object DefaultFilterHelper extends ExpenseFilterHelper {

    override val defaultOps: Map[String, (Operation, Extractor)] = Map(
      Field.Id -> (EQUAL, extractLong _),
      Field.CreationDate -> (LIKE, extractString _),
      Field.Description -> (LIKE, extractString _),
      Field.Amount -> (EQUAL, extractBigDecimal _),
      Field.UserId -> (EQUAL, extractLong _),
      Field.Comment -> (LIKE, extractString _)
    )

    private def extractBigDecimal(input: String): Option[java.math.BigDecimal] = Try(BigDecimal(input).bigDecimal).toOption

    private def extractLong(input: String): Option[Long] = Try(input.toLong).toOption

    private def extractString(input: String): Option[String] = Some(input)
    
  }
}
