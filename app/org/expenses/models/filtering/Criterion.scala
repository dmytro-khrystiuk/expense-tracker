package org.expenses.models.filtering

import org.expenses.models.filtering.Operation.Operation

case class Criterion(field: String, operation: Operation, value: Any)
