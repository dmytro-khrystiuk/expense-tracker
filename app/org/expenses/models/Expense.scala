package org.expenses.models

import java.util.Date
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.JsString

case class Expense(id: Option[Long],
                   creationDate: Date,
                   description: String,
                   amount: BigDecimal,
                   userId: Long,
                   comment: Option[String] = None) {

  override def hashCode(): Int = super.hashCode()

  override def equals(other: Any): Boolean = {
    val e = other.asInstanceOf[Expense]
    e.id == id &&
    e.creationDate.getTime == creationDate.getTime &&
    e.description == description &&
    e.amount == amount &&
    e.comment == comment
  }

}

object Expense {
  val DatePattern = "yyyy/MM/dd HH:mm"
  
  object Field {
    val Id = "id"
    val CreationDate = "creation_date"
    val Description = "description"
    val Amount = "amount"
    val UserId = "user_id"
    val Comment = "comment"
  }


  implicit object DateWrites extends Writes[Date] {
    def writes(date: Date): JsValue = JsString(new java.text.SimpleDateFormat(DatePattern).format(date))
  }

  implicit object IsoDateReads extends Reads[Date] with DefaultReads {
    override def reads(json: JsValue): JsResult[Date] = dateReads("yyyy-MM-dd'T'HH:mm:ss.SSSZ", input =>
      if (input.endsWith("Z")) {
        input.substring(0, input.length() - 1) + "GMT-00:00"
      } else {
        val inset = 6

        val s0 = input.substring(0, input.length - inset)
        val s1 = input.substring(input.length - inset, input.length)

        s0 + "GMT" + s1
      }).reads(json)
  }

  implicit object ExpenseFormat extends Format[Expense] {

    override def reads(json: JsValue): JsResult[Expense] = (
      (JsPath \ "id").readNullable[Long](min(1L)) and
      (JsPath \ "creationDate").read[Date] and
      (JsPath \ "description").read[String](minLength[String](1)) and
      (JsPath \ "amount").read[BigDecimal](min(BigDecimal(0))) and
      (JsPath \ "userId").read[Long](min(1L)) and
      (JsPath \ "comment").readNullable[String]
    )(Expense.apply _).reads(json)

    override def writes(expense: Expense): JsValue = Json.writes[Expense].writes(expense)

  }

}