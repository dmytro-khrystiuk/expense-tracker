package org.expenses.services

import org.expenses.dao.SqlExpenseDaoComponent
import org.expenses.models.filtering.DefaultExpenseFilterComponent

trait DefaultExpenseServiceComponent extends ExpenseServiceComponent
                                     with SqlExpenseDaoComponent
                                     with DefaultExpenseFilterComponent {
  override val expenseService: ExpenseService = new ExpenseService
}
