package org.expenses.services

import org.expenses.models.User
import org.expenses.dao.UserDaoComponent
import org.expenses.controllers.security.SecurityHelperComponent

trait UserServiceComponent { this: UserDaoComponent with SecurityHelperComponent =>
  val userService: UserService

  class UserService {
    def authenticate(login: String, password: String): Option[User] = userDao.getUserByLogin(login) filter { user =>
      securityHelper.checkPassword(password, user.password)
    }

    def register(user: User): Option[User] =
      userDao.create(User(None, user.login, user.name, securityHelper.hashPassword(user.password)))

  }

}
