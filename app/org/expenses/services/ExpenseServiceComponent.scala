package org.expenses.services

import org.expenses.dao.ExpenseDaoComponent
import org.expenses.models.Expense
import scala.util.Try
import org.expenses.models.filtering.{OrderBy, ExpenseFilterComponent}
import java.util.Date
import org.expenses.models.filtering.Operation._
import org.expenses.models.Expense.Field._
import scala.util.Failure

trait ExpenseServiceComponent { this: ExpenseDaoComponent with ExpenseFilterComponent =>
  val expenseService: ExpenseService

  class ExpenseService {
    lazy val DefaultOrder = OrderBy(OrderBy.Desc, CreationDate)

    def getExpense(id: Long, userId: Long): Option[Expense] = {
      val filter = newExpenseFilter.withCriterion(UserId, EQUAL, userId)
                                   .withCriterion(Id, EQUAL, id)
      expenseDao.list(filter, DefaultOrder).headOption
    }

    def expensesOfUser(userId: Long, filter: ExpenseFilter, order: Option[String]): Seq[Expense] = {
      val orderBy = order.fold(DefaultOrder)(OrderBy(_))
      expenseDao.list(filter.withCriterion(UserId, EQUAL, userId), orderBy)
    }

    def createExpense(expense: Expense, userId: Long): Try[Expense] = createOrUpdate(expense, userId)(expenseDao.create)

    def updateExpense(expense: Expense, userId: Long): Try[Expense] = createOrUpdate(expense, userId)(expenseDao.update)

    def reportForDateRange(userId: Long, startDate: Date, endDate: Date): (Seq[Expense], BigDecimal, BigDecimal) = {
      val filter = newExpenseFilter.withCriterion(UserId, EQUAL, userId)
                                   .withCriterion(CreationDate, GREATER_THAN_OR_EQUAL, startDate)
                                   .withCriterion(CreationDate, LESS_THAN_OR_EQUAL, endDate)

      val expenses = expenseDao.list(filter, DefaultOrder)

      if (!expenses.isEmpty) {
        val (total, dayCount) = expenseDao.getAmountAndDayCount(filter)

        (expenses, total, total / dayCount)
      } else {
        (Nil, BigDecimal(0), BigDecimal(0))
      }

    }

    private def createOrUpdate(expense: Expense, userId: Long)(function: Expense => Try[Expense]): Try[Expense] = {
      if (userId == expense.userId)
        function(expense)
      else
        Failure(new IllegalArgumentException("Current userId does not match to userId of expense"))
    }

  }

}
