package org.expenses.services

import org.expenses.dao.SqlUserUserDaoComponent
import org.expenses.controllers.security.DefaultSecurityHelperComponent

trait DefaultUserServiceComponent extends UserServiceComponent
                                  with SqlUserUserDaoComponent
                                  with DefaultSecurityHelperComponent {
  override val userService = new UserService
}
