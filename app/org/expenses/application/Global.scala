package org.expenses.application

import scala.concurrent.Future
import play.api.libs.json.Json
import play.api.mvc._
import play.api._
import Results._
import play.api.mvc.PathBindable.Parsing
import java.util.Date
import java.text.SimpleDateFormat

object Global extends GlobalSettings {

  def errorToJson(message: String) = Json.obj("error" -> message)

  def badRequest(message: String) = BadRequest(errorToJson(message))

  def internalError(exception: Throwable) = InternalServerError(errorToJson(exception.getMessage))

  override def onError(request: RequestHeader, ex: Throwable): Future[SimpleResult] = Future.successful {
    Logger.error(ex.getMessage, ex)
    internalError(ex)
  }

  override def onBadRequest(request: RequestHeader, error: String): Future[SimpleResult] = Future.successful {
    badRequest(error)
  }

  override def onHandlerNotFound(request: RequestHeader): Future[SimpleResult] = Future.successful {
    NotFound(errorToJson(s"Cannot serve uri '${request.uri}'"))
  }

  def camelToUnderscores(value: String) = "[A-Z\\d]".r.replaceAllIn(value, {m =>
    "_" + m.group(0).toLowerCase
  })

  def underscoreToCamel(value: String) = "_([a-z\\d])".r.replaceAllIn(value, {m =>
    m.group(1).toUpperCase
  })

  implicit object BindableDate extends Parsing[Date](
    string => new SimpleDateFormat("yyyy-MM-dd").parse(string),
    date => date.toString,
    (key: String, e: Exception) => "Cannot parse parameter %s as Date: %s".format(key, e.getMessage)
  )

}
