package org.expenses.dao

import org.expenses.models.Expense
import anorm._
import anorm.SqlParser._
import org.expenses.models.Expense._
import scala.util.Try
import org.expenses.models.filtering.{OrderBy, DefaultExpenseFilterComponent}
import org.expenses.models.filtering.Operation.LIKE
import play.api.db.DB._
import org.expenses.models.filtering.Criterion
import scala.Some
import anorm.~
import play.api.Play.current

trait SqlExpenseDaoComponent extends ExpenseDaoComponent with DefaultExpenseFilterComponent {
  override val expenseDao: ExpenseDao = SqlExpenseDao

  object SqlExpenseDao extends ExpenseDao {

    private val expenseParser = get[Option[Long]](Field.Id) ~
      date(Field.CreationDate) ~
      str(Field.Description) ~
      get[java.math.BigDecimal](Field.Amount) ~
      long(Field.UserId) ~
      get[Option[String]](Field.Comment) map {
      case id ~ creationDate ~ description ~ amount ~ userId ~ comment =>
        Expense(id, creationDate, description, amount, userId, comment)
    }

    override def list(filter: ExpenseFilter, orderBy: OrderBy): Seq[Expense] =
      withTransaction { implicit connection =>
        SQL(
          """
          SELECT
            id, creation_date, description, amount, user_id, comment
          FROM
            EXPENSE
          """ + whereClause(filter) + orderByClause(orderBy)
        ).on(filterParameters(filter): _*).as(expenseParser *)
      }

    override def create(expense: Expense): Try[Expense] = withTransaction { implicit connection =>
      Try(SQL(
      """
        INSERT INTO EXPENSE (creation_date, description, amount, user_id, comment)
        VALUES({creation_date}, {description}, {amount}, {user_id}, {comment})
      """
      ).on(expenseParameters(expense): _*)
       .executeInsert(scalar[Long].single)).map { id =>
          Expense(Some(id), expense.creationDate, expense.description, expense.amount, expense.userId, expense.comment)
       }
    }

    override def update(expense: Expense): Try[Expense] = withTransaction { implicit connection =>
      Try {
        val updatedRows =
          SQL("""UPDATE EXPENSE
             SET creation_date = {creation_date},
                 description = {description},
                 amount = {amount},
                 comment = {comment}
             WHERE id = {id}"""
          ).on(expenseParameters(expense): _*)
           .executeUpdate()
        if (updatedRows == 0)
          throw new IllegalArgumentException(s"Expense with id=${expense.id} does not exist.")
        else
          expense
      }
    }

    override def getAmountAndDayCount(filter: ExpenseFilter): (BigDecimal, Long) = withTransaction { implicit connection =>
      SQL("""
          SELECT
            SUM(AMOUNT) amount, COUNT(DISTINCT PARSEDATETIME(CREATION_DATE, 'yyyy-MM-dd')) days, user_id
          FROM
            EXPENSE
        """ + whereClause(filter) + "GROUP BY user_id"
      ).on(filterParameters(filter): _*)
       .as(get[java.math.BigDecimal](Field.Amount) ~ long("days") ~ long(Field.UserId) *)
       .map { case amount ~ days ~ userId => (BigDecimal(amount), days) }.headOption.getOrElse((BigDecimal(0), 0L))
    }

    private def expenseParameters(expense: Expense) = {
      Seq('id -> expense.id,
          'creation_date -> expense.creationDate,
          'description -> expense.description,
          'amount -> expense.amount.bigDecimal,
          'user_id -> expense.userId,
          'comment -> expense.comment).map { case (key, value) => key -> toParameterValue(value) }
    }

    private def whereClause(filter: ExpenseFilter) = {
      val whereList = filter.criteria map { criterion =>
        s"${criterion.field} ${criterion.operation} {${placeholder(criterion)}}"
      }
      if (!whereList.isEmpty) whereList.mkString(" WHERE ", " AND ", "") else ""
    }

    private def filterParameters(filter: ExpenseFilter) = filter.criteria.map {
      case criterion @ Criterion(field, operation, value) if operation == LIKE => placeholder(criterion) -> toParameterValue(s"%$value%")
      case criterion @ Criterion(field, _, value) => placeholder(criterion) -> toParameterValue(value)
    }.toSeq

    private def orderByClause(orderBy: OrderBy) = s" ORDER BY ${orderBy.field} ${orderBy.direction}"

    private def placeholder(criterion: Criterion) = criterion.field + criterion.hashCode.abs.toString

  }

}
