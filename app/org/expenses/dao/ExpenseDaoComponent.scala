package org.expenses.dao

import org.expenses.models.Expense
import scala.util.Try
import org.expenses.models.filtering.{OrderBy, ExpenseFilterComponent}

trait ExpenseDaoComponent { this: ExpenseFilterComponent =>
  val expenseDao: ExpenseDao

  trait ExpenseDao {

    def list(filter: ExpenseFilter, orderBy: OrderBy): Seq[Expense]

    def create(expense: Expense): Try[Expense]

    def update(expense: Expense): Try[Expense]

    def getAmountAndDayCount(filter: ExpenseFilter): (BigDecimal, Long)

  }

}
