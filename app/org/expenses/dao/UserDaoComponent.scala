package org.expenses.dao

import org.expenses.models.User

trait UserDaoComponent {
  val userDao: UserDao

  trait UserDao {
    def getUserByLogin(login: String): Option[User]

    def create(user: User): Option[User]
  }

}
