package org.expenses.dao

import org.expenses.models.User
import anorm._
import anorm.SqlParser.scalar
import scala.util.Try
import play.api.db.DB.withTransaction
import play.api.Play.current

trait SqlUserUserDaoComponent extends UserDaoComponent {
  override val userDao = SqlUserDao

  object SqlUserDao extends UserDao {

    override def getUserByLogin(login: String): Option[User] = withTransaction { implicit connection => 
      SQL(
        """SELECT
             id, login, name, password
           FROM
             "USER"
           WHERE
             login = {login}"""
      ).on('login -> login)
       .singleOpt
       .collect { case Row(id: Long, _, name: String, password: String) => User(Some(id), login, name, password) }
    }

    override def create(user: User): Option[User] = withTransaction { implicit connection =>
      Try(SQL(
        """INSERT INTO "USER"(login, name, password) VALUES({login}, {name}, {password})"""
      ).on('login -> user.login, 'name -> user.name, 'password -> user.password)
       .executeInsert(scalar[Long].single)).toOption
       .collect { case id => User(Some(id), user.login, user.name, user.password)}
    }

  }

}
