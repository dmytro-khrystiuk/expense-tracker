package org.expenses.controllers

import play.api.mvc._
import org.expenses.controllers.security.{UserInfo, Authenticated}
import play.api.libs.json.{JsValue, Json}
import org.expenses.services.{DefaultExpenseServiceComponent, ExpenseServiceComponent}
import org.expenses.models.Expense
import org.expenses.application.Global._
import scala.util.{Try, Success, Failure}
import play.api.mvc.{AnyContent, SimpleResult}
import play.api.mvc.Security.AuthenticatedRequest
import org.expenses.models.filtering.{DefaultExpenseFilterComponent, ExpenseFilterComponent}
import java.util.Date

object ExpenseController extends ExpenseController with DefaultExpenseServiceComponent with DefaultExpenseFilterComponent

trait ExpenseController extends Controller { this: ExpenseServiceComponent with ExpenseFilterComponent =>
  private val MandatoryFieldsMessage = "Expense must have 'creationDate', 'description' and positive 'amount'"
  private val ExpenseDoesNotExistMessage = "Expense for requested id does not exist"

  def expense(id: Long) = Authenticated { request =>
    expenseService.getExpense(id, request.user.id).fold(badRequest(ExpenseDoesNotExistMessage)) {
      expense => Ok(Json.toJson(expense))
    }
  }

  def expenses = Authenticated { request =>
    Ok(Json.toJson(expenseService.expensesOfUser(request.user.id, filterForRequest(request), request.getQueryString("orderBy"))))
  }

  def createExpense = Authenticated(parse.json)(processCreateOrUpdate(_)(expenseService.createExpense))

  def modifyExpense = Authenticated(parse.json)(processCreateOrUpdate(_)(expenseService.updateExpense))

  def reportForRange(startDate: Date, endDate: Date) = Authenticated { request =>
    val (expenses, total, average) = expenseService.reportForDateRange(request.user.id, startDate, endDate)
    Ok(Json.obj(
      "expenses" -> expenses,
      "total" -> total,
      "average" -> average
    ))
  }

  private def processCreateOrUpdate(request: AuthenticatedRequest[JsValue, UserInfo])
                                   (function: (Expense, Long) => Try[Expense]): SimpleResult = {
    request.body.validate[Expense].map { expense =>
      function(expense, request.user.id) match {
        case Success(result) => Ok(Json.toJson(result))
        case Failure(ex: IllegalArgumentException) => badRequest(ex.getMessage)
        case Failure(ex) => internalError(ex)
      }
    }.recoverTotal { _ => badRequest(MandatoryFieldsMessage) }
  }

  private def filterForRequest(request: AuthenticatedRequest[AnyContent, UserInfo]) = {
    expenseFilterHelper.FilterFields.foldLeft(newExpenseFilter) {
      (filter, field) => request.getQueryString(field).fold(filter)(filter.withCriterion(field, _))
    }
  }

}
