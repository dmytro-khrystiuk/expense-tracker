package org.expenses.controllers

import play.api.mvc._
import controllers.Assets
import scala.concurrent.Await
import scala.concurrent.duration._

object Application extends Controller {

  def index = Action { request =>
    Await.result(Assets.at("/public/app", "index.html").apply(request), 10 seconds)
  }

}