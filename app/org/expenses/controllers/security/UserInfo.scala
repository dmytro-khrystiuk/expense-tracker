package org.expenses.controllers.security

import play.api.mvc.RequestHeader

case class UserInfo(id: Long, name: String)

object UserInfo {

  def fromRequest(request: RequestHeader): Option[UserInfo] = {
    val userIdOption = request.session.get(SessionKey.UserId).map(_.toLong)
    val nameOption = request.session.get(SessionKey.Name)
    (userIdOption, nameOption) match {
      case (Some(userId), Some(name)) => Some(UserInfo(userId, name))
      case _ => None
    }
  }

  object SessionKey {
    val UserId = "userId"
    val Name = "name"
  }

}
