package org.expenses.controllers.security

trait DefaultSecurityHelperComponent extends SecurityHelperComponent {
  override val securityHelper: SecurityHelper = new SecurityHelper
}
