package org.expenses.controllers.security

import org.mindrot.jbcrypt.BCrypt

trait SecurityHelperComponent {
  val securityHelper: SecurityHelper

  class SecurityHelper {
    def hashPassword(password: String) = BCrypt.hashpw(password, BCrypt.gensalt())

    def checkPassword(plaintext: String, hashed: String) = BCrypt.checkpw(plaintext, hashed)
  }

}
