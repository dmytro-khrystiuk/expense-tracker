package org.expenses.controllers.security

import play.api.mvc.Security.AuthenticatedBuilder
import org.expenses.controllers.security.UserInfo._
import org.expenses.application.Global._
import org.expenses.controllers.Application.Unauthorized

object Authenticated extends AuthenticatedBuilder(request => fromRequest(request),
                                                  _ => Unauthorized(errorToJson("Authorization required")))


