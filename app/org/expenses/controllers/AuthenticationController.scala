package org.expenses.controllers

import play.api.mvc._
import org.expenses.services.{DefaultUserServiceComponent, UserServiceComponent}
import play.api.libs.json.Reads._
import org.expenses.models.User
import org.expenses.controllers.security.UserInfo.SessionKey
import org.expenses.controllers.security.{UserInfo, Authenticated}
import play.api.libs.json.Json
import org.expenses.application.Global._
import scala.Some

object AuthenticationController extends AuthenticationController with DefaultUserServiceComponent

trait AuthenticationController extends Controller { this: UserServiceComponent =>
  private val InvalidRegistrationData = "'login', 'name' and 'password' fields are mandatory"
  private val InvalidAuthData = "Login and password should not be empty"
  private val UserExists = "User with login '%s' is already exists"
  private val InvalidCredentials = "Username or password are incorrect"

  def authenticate = Action(parse.json) { request =>
    val loginOption = (request.body \ User.Field.Login).asOpt[String]
    val passwordOption = (request.body \ User.Field.Password).asOpt[String]

    (loginOption, passwordOption) match {
      case (Some(login), Some(password)) if !login.isEmpty && !password.isEmpty =>
        userService.authenticate(login, password).fold(Unauthorized(errorToJson(InvalidCredentials)))(success)
      case _ => badRequest(InvalidAuthData)
    }
  }

  def register = Action(parse.json) { request =>
    request.body.validate[User].map { user =>
      userService.register(user).fold(badRequest(UserExists.format(user.login)))(success)
    }.recoverTotal { _ => badRequest(InvalidRegistrationData) }
  }

  def logout = Action(Ok.withNewSession)

  def currentUser = Authenticated { request =>
    implicit val userInfoWrites = Json.writes[UserInfo]
    Ok(Json.toJson(request.user))
  }

  private def success(user: User) = {
    Ok(Json.toJson(user)).withSession(SessionKey.UserId -> user.id.get.toString,
                                      SessionKey.Name -> user.name)
  }

}
