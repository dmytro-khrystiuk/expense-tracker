package org.expenses.models.filtering

import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class OrderByTest extends FunSuite {
  private val Field = "someField"
  private val ExpectedField = "some_field"

  test("apply should return Asc order for field when input prefixed with +") {
    assert(OrderBy("ASC", ExpectedField) === OrderBy(s"+$Field"))
  }

  test("apply should return Asc order for field when input is without prefix") {
    assert(OrderBy("ASC", ExpectedField) === OrderBy(Field))
  }

  test("apply should return Desc order for field when input prefixed with -") {
    assert(OrderBy("DESC", ExpectedField) === OrderBy(s"-$Field"))
  }

}
