package org.expenses.models.filtering

import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.scalatest.{FunSuite, BeforeAndAfter}
import org.mockito.Mockito._
import org.expenses.models.filtering.Operation._
import scala.Some

@RunWith(classOf[JUnitRunner])
class ExpenseFilterTest extends FunSuite with BeforeAndAfter with ExpenseFilterComponent {
  private val ExpectedOperation = Operation.EQUAL
  private val ProvidedOperation = Operation.LIKE
  private val ExpectedField = "expected_field"
  private val ExpectedValue = "ExpectedValue"
  private val InputField = "expectedField"
  private val ExpectedCriterion = Criterion(ExpectedField, ExpectedOperation, ExpectedValue)

  override def newExpenseFilter: ExpenseFilter = new ExpenseFilter(Set.empty)
  override val expenseFilterHelper: ExpenseFilterHelper = mock(classOf[ExpenseFilterHelper])



  test("should return filter with expected criteria when field exists in mapping") {
    // Given
    when(expenseFilterHelper.defaultOps).thenReturn(provideMapping())
    // When
    val filter = newExpenseFilter.withCriterion(InputField, ExpectedOperation, ExpectedValue)
    // Then
    assert(Set(ExpectedCriterion) === filter.criteria)
  }

  test("should return filter with the same criteria when field does not exist in mapping and any value or operation provided") {
    // Given
    when(expenseFilterHelper.defaultOps).thenReturn(Map.empty[String, (Operation, Extractor)])
    // When Then
    assert(Set.empty[Criterion] === newExpenseFilter.withCriterion(InputField, ExpectedOperation, ExpectedValue).criteria)
    assert(Set.empty[Criterion] === newExpenseFilter.withCriterion(InputField, ExpectedValue).criteria)
    assert(Set.empty[Criterion] === newExpenseFilter.withCriterion(InputField, s"$ProvidedOperation $ExpectedValue").criteria)
  }

  test("should return filter with expected criteria and default operation when field exists in mapping and operation did not provided in input") {
    // Given
    when(expenseFilterHelper.defaultOps).thenReturn(provideMapping())
    // When
    val filter = newExpenseFilter.withCriterion(InputField, ExpectedValue)
    // Then
    assert(Set(ExpectedCriterion) === filter.criteria)
  }

  test("should return filter with expected criteria and given operation when field exists in mapping and operation provided in input") {
    // Given
    when(expenseFilterHelper.defaultOps).thenReturn(provideMapping())
    // When
    val filter = newExpenseFilter.withCriterion(InputField, s"$ProvidedOperation $ExpectedValue")
    // Then
    assert(Set(Criterion(ExpectedField, ProvidedOperation, ExpectedValue)) === filter.criteria)
  }

  private def provideMapping(field: String = ExpectedField,
                             operation: Operation = ExpectedOperation,
                             extractor: Extractor = _ => Some(ExpectedValue)) = Map(field -> (operation, extractor))

}
