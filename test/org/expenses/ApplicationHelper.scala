package org.expenses

import play.api.test.Helpers._
import java.sql.Connection
import anorm._
import play.api.test.FakeApplication
import org.expenses.models.{User, Expense}

trait ApplicationHelper {

  def fakeApplication = {
    FakeApplication(additionalConfiguration = inMemoryDatabase(options = Map("MODE" -> "MYSQL", "DB_CLOSE_DELAY" -> "-1")))
  }

  def userCount(implicit connection: Connection) = SQL("SELECT COUNT(*) FROM \"USER\"").as(SqlParser.scalar[Long].single)

  def expenseCount(implicit connection: Connection) = SQL("SELECT COUNT(*) FROM EXPENSE").as(SqlParser.scalar[Long].single)

  def persistUser(user: User)(implicit connection: Connection) {
    SQL(s"""INSERT INTO "USER"(login, name, password) VALUES('${user.login}', '${user.name}', '${user.password}')""").asSimple.execute()
  }

  def persistExpense(expense: Expense)(implicit connection: Connection) {
    SQL(
      s"""
        INSERT INTO EXPENSE (creation_date, description, amount, user_id)
        VALUES({creationDate}, '${expense.description}', ${expense.amount}, ${expense.userId})
      """
    ).on('creationDate -> expense.creationDate).execute()
  }

}
