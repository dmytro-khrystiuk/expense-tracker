package org.expenses.application

import play.api.test.Helpers._
import org.specs2.runner._
import org.junit.runner._

import play.api.mvc.{RequestHeader, SimpleResult}
import scala.concurrent.Future
import org.mockito.Mockito._
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class GlobalTest extends FunSuite {
  private val Message = "SomeMessage"
  private val ExpectedJsonString = s"""{"error":"$Message"}"""
  private val CamelString = "someString"
  private val UnderscoredString = "some_string"

  test("errorToJson should return given message as json") {
    assert(ExpectedJsonString === Global.errorToJson(Message).toString)
  }

  test("badRequest should return result with status 400 and message as json") {
    // When
    val result = Global.badRequest(Message)
    // Then
    assert(contentAsString(wrap(result)).contains(ExpectedJsonString))
    assert(400 === result.header.status)
  }

  test("internalError should return result with status 500 and message as json") {
    // Given
    val ex = mock(classOf[Exception])
    when(ex.getMessage).thenReturn(Message)
    // When
    val result = Global.internalError(ex)
    // Then
    assert(contentAsString(wrap(result)).contains(ExpectedJsonString))
    assert(500 === result.header.status)
  }

  test("onError should return result with status 500 and message as json") {
    // Given
    val ex = mock(classOf[Exception])
    when(ex.getMessage).thenReturn(Message)
    // When
    val result = Global.onError(mock(classOf[RequestHeader]), ex)
    // Then
    assert(contentAsString(result).contains(ExpectedJsonString))
    assert(500 === status(result))
  }

  test("onBadRequest should return result with status 400 and message as json") {
    // When
    val result = Global.onBadRequest(mock(classOf[RequestHeader]), Message)
    // Then
    assert(contentAsString(result).contains(ExpectedJsonString))
    assert(400 === status(result))
  }

  test("onHandlerNotFound should return result with status 404 and message as json") {
    // Given
    val request = mock(classOf[RequestHeader])
    when(request.uri).thenReturn(Message)
    // When
    val result = Global.onHandlerNotFound(request)
    // Then
    assert(contentAsString(result).contains(s"Cannot serve uri '$Message'"))
    assert(404 === status(result))
  }

  test("camelToUnderscores should return string with underscores instead of capital letters") {
    assert(UnderscoredString === Global.camelToUnderscores(CamelString))
  }

  test("underscoreToCamel should return string in camel case instead of underscores") {
    assert(CamelString === Global.underscoreToCamel(UnderscoredString))
  }

  private def wrap(result: SimpleResult) = Future.successful(result)

}
