package org.expenses.services

import org.expenses.dao.UserDaoComponent
import org.scalatest.{FunSuite, BeforeAndAfter}
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.expenses.controllers.security.SecurityHelperComponent
import org.mockito.Mockito._
import org.expenses.models.User
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import org.mockito.ArgumentCaptor
import org.mockito.Matchers.any

@RunWith(classOf[JUnitRunner])
class UserServiceTest extends FunSuite
                      with BeforeAndAfter
                      with UserServiceComponent
                      with UserDaoComponent
                      with SecurityHelperComponent {
  private val Login = "Login"
  private val Name = "Name"
  private val Password = "Password"
  private val HashedPassword = "HashedPassword"
  private val ExpectedUser = User(None, Login, Name, HashedPassword)

  override val userService: UserService = new UserService

  override val userDao: UserDao = mock(classOf[UserDao])
  override val securityHelper: SecurityHelper = mock(classOf[SecurityHelper])

  before {
    reset(userDao, securityHelper)
  }


  test("authenticate should return user when it passed security check") {
    // Given
    when(userDao.getUserByLogin(Login)).thenReturn(Some(ExpectedUser))
    when(securityHelper.checkPassword(Password, HashedPassword)).thenReturn(true)
    // When
    val Some(user) = userService.authenticate(Login, Password)

    assert(ExpectedUser === user)
    verify(userDao).getUserByLogin(Login)
    verify(securityHelper).checkPassword(Password, HashedPassword)
    verifyNoMoreInteractions(userDao, securityHelper)
  }

  test("authenticate should return None when user did not passed security check") {
    // Given
    when(userDao.getUserByLogin(Login)).thenReturn(Some(ExpectedUser))
    when(securityHelper.checkPassword(Password, HashedPassword)).thenReturn(false)
    // When
    val user = userService.authenticate(Login, Password)

    assert(None === user)
    verify(userDao).getUserByLogin(Login)
    verify(securityHelper).checkPassword(Password, HashedPassword)
    verifyNoMoreInteractions(userDao, securityHelper)
  }

  test("authenticate should return None when userDao returned None") {
    // Given
    when(userDao.getUserByLogin(Login)).thenReturn(None)
    // When
    val user = userService.authenticate(Login, Password)

    assert(None === user)
    verify(userDao).getUserByLogin(Login)
    verifyNoMoreInteractions(userDao)
    verifyZeroInteractions(securityHelper)
  }

  test("register should return created user when userDao returned it") {
    // Given
    val userToRegister = User(None, Login, Name, Password)
    val userCaptor = ArgumentCaptor.forClass(classOf[User])
    when(userDao.create(any[User])).thenAnswer(new Answer[Option[User]] {
      override def answer(invocation: InvocationOnMock): Option[User] =
        Some(invocation.getArguments.head.asInstanceOf[User])
    })
    when(securityHelper.hashPassword(Password)).thenReturn(HashedPassword)
    // When
    val Some(user) = userService.register(userToRegister)
    // Then
    assert(ExpectedUser === user)
    verify(userDao).create(userCaptor.capture())
    verify(securityHelper).hashPassword(Password)
    verifyNoMoreInteractions(userDao, securityHelper)
    assert(ExpectedUser === userCaptor.getValue)
  }

}
