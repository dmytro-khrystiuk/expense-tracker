package org.expenses.services

import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.scalatest.{BeforeAndAfter, FunSuite}
import org.expenses.dao.ExpenseDaoComponent
import org.expenses.models.filtering.{ExpenseFilterComponent, OrderBy}
import org.mockito.ArgumentCaptor
import org.mockito.Mockito._
import org.mockito.Matchers._
import org.expenses.models.filtering.Operation._
import org.mockito.stubbing.Answer
import org.mockito.invocation.InvocationOnMock
import java.util.Date
import org.expenses.models.Expense
import org.expenses.models.Expense._
import org.expenses.models.filtering.OrderBy._
import org.expenses.models.filtering.Operation.Operation
import scala.util.Try
import scala.util.Failure
import scala.Some
import scala.util.Success

@RunWith(classOf[JUnitRunner])
class ExpenseServiceTest extends FunSuite
                         with BeforeAndAfter
                         with ExpenseServiceComponent
                         with ExpenseDaoComponent
                         with ExpenseFilterComponent {
  private val Id = 1L
  private val CreationDate = new Date(1L)
  private val Description = "Description"
  private val Amount = BigDecimal(100)
  private val UserId = 2L
  private val ExpectedExpense = Expense(Some(Id), CreationDate, Description, Amount, UserId)
  private val ExpectedOrder = OrderBy(Desc, Field.CreationDate)
  private val TotalAmount = BigDecimal(5000)
  private val DayCount = 25L
  private val StartDate = new Date(10L)
  private val EndDate = new Date(20L)

  override val expenseService: ExpenseService = new ExpenseService

  override val expenseDao: ExpenseDao = mock(classOf[ExpenseDao])
  override val newExpenseFilter: ExpenseFilter = mock(classOf[ExpenseFilter])
  override val expenseFilterHelper: ExpenseFilterHelper = mock(classOf[ExpenseFilterHelper])

  private var fieldCaptor: ArgumentCaptor[String] = _
  private var operationCaptor: ArgumentCaptor[Operation] = _
  private var valueCaptor: ArgumentCaptor[Any] = _


  before {
    reset(expenseDao, newExpenseFilter, expenseFilterHelper)

    fieldCaptor = ArgumentCaptor.forClass(classOf[String])
    operationCaptor = ArgumentCaptor.forClass(classOf[Operation])
    valueCaptor = ArgumentCaptor.forClass(classOf[Any])

    when(newExpenseFilter.withCriterion(any[String], any[Operation], any)).thenAnswer(new Answer[ExpenseFilter] {
      override def answer(invocation: InvocationOnMock): ExpenseFilter = invocation.getMock.asInstanceOf[ExpenseFilter]
    })
  }

  test("getExpense should return expense") {
    // Given
    when(expenseDao.list(newExpenseFilter, ExpectedOrder)).thenReturn(List(ExpectedExpense))
    // When
    val Some(expense) = expenseService.getExpense(Id, UserId)
    // Then
    assert(ExpectedExpense === expense)
    verify(expenseDao).list(newExpenseFilter, ExpectedOrder)
    verifyFilterInvocations(2)
    assert(List(Field.UserId, Field.Id) === values(fieldCaptor))
    assert(values(operationCaptor).forall(_ == EQUAL))
    assert(List(UserId, Id) === values(valueCaptor))
  }

  test("expensesOfUser should return list of Expenses with given order when it is provided") {
    // Given
    val orderString = "someString"
    val orderingCaptor = ArgumentCaptor.forClass(classOf[OrderBy])
    when(expenseDao.list(any[ExpenseFilter], orderingCaptor.capture())).thenReturn(List(ExpectedExpense))
    // When
    val expenses = expenseService.expensesOfUser(UserId, newExpenseFilter, Some(orderString))
    // Then
    assert(List(ExpectedExpense) === expenses)
    verify(expenseDao).list(newExpenseFilter, orderingCaptor.getValue)
    verifyFilterInvocations(1)
    assert(Field.UserId === fieldCaptor.getValue)
    assert(EQUAL === operationCaptor.getValue)
    assert(UserId === valueCaptor.getValue)
  }

  test("expensesOfUser should return list of Expenses with default order when it is not provided") {
    // Given
    when(expenseDao.list(newExpenseFilter, ExpectedOrder)).thenReturn(List(ExpectedExpense))
    // When
    val expenses = expenseService.expensesOfUser(UserId, newExpenseFilter, None)
    // Then
    assert(List(ExpectedExpense) === expenses)
    verify(expenseDao).list(newExpenseFilter, ExpectedOrder)
    verifyFilterInvocations(1)
    assert(Field.UserId === fieldCaptor.getValue)
    assert(EQUAL === operationCaptor.getValue)
    assert(UserId === valueCaptor.getValue)
  }

  test("createExpense should return expense provided by dao") {
    // Given
    when(expenseDao.create(any[Expense])).thenAnswer(new Answer[Try[Expense]] {
      override def answer(invocation: InvocationOnMock): Try[Expense] =
        Success(invocation.getArguments.head.asInstanceOf[Expense])
    })
    // When
    val Success(expense) = expenseService.createExpense(ExpectedExpense, UserId)
    // Then
    assert(ExpectedExpense === expense)
    verify(expenseDao).create(ExpectedExpense)
  }

  test("updateExpense should return expense provided by dao") {
    // Given
    when(expenseDao.update(any[Expense])).thenAnswer(new Answer[Try[Expense]] {
      override def answer(invocation: InvocationOnMock): Try[Expense] =
        Success(invocation.getArguments.head.asInstanceOf[Expense])
    })
    // When
    val Success(expense) = expenseService.updateExpense(ExpectedExpense, UserId)
    // Then
    assert(ExpectedExpense === expense)
    verify(expenseDao).update(ExpectedExpense)
  }

  test("createExpense and updateExpense should return Failure when UserId in expense does not match to second argument") {
    // Given
    val otherId = UserId + 10
    // When
    val Failure(createResult) = expenseService.createExpense(ExpectedExpense, otherId)
    val Failure(updateResult) = expenseService.updateExpense(ExpectedExpense, otherId)
    // Then
    assert(createResult.isInstanceOf[IllegalArgumentException])
    assert(updateResult.isInstanceOf[IllegalArgumentException])
  }

  test("reportForDateRange should return expenses, their total amount and average by day when expenses returned by dao") {
    // Given
    when(expenseDao.list(newExpenseFilter, ExpectedOrder)).thenReturn(List(ExpectedExpense))
    when(expenseDao.getAmountAndDayCount(newExpenseFilter)).thenReturn((TotalAmount, DayCount))
    // When
    val (expenses, total, average) = expenseService.reportForDateRange(UserId, StartDate, EndDate)
    // Then
    assert(List(ExpectedExpense) === expenses)
    assert(TotalAmount === total)
    assert(TotalAmount / DayCount === average)

    verifyFilterInvocations(3)
    assert(List(Field.UserId, Field.CreationDate, Field.CreationDate) === values(fieldCaptor))
    assert(List(EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL) === values(operationCaptor))
    assert(List(UserId, StartDate, EndDate) === values(valueCaptor))

    verify(expenseDao).list(newExpenseFilter, ExpectedOrder)
    verify(expenseDao).getAmountAndDayCount(newExpenseFilter)
    verifyNoMoreInteractions(newExpenseFilter, expenseDao)
  }

  test("reportForDateRange should return empty list, zero total amount and average by day when expenses did not returned by dao") {
    // Given
    when(expenseDao.list(newExpenseFilter, ExpectedOrder)).thenReturn(Nil)
    // When
    val (expenses, total, average) = expenseService.reportForDateRange(UserId, StartDate, EndDate)
    // Then
    assert(Nil === expenses)
    assert(BigDecimal(0) === total)
    assert(BigDecimal(0) === average)

    verifyFilterInvocations(3)
    assert(List(Field.UserId, Field.CreationDate, Field.CreationDate) === values(fieldCaptor))
    assert(List(EQUAL, GREATER_THAN_OR_EQUAL, LESS_THAN_OR_EQUAL) === values(operationCaptor))
    assert(List(UserId, StartDate, EndDate) === values(valueCaptor))

    verify(expenseDao).list(newExpenseFilter, ExpectedOrder)
    verifyNoMoreInteractions(newExpenseFilter, expenseDao)
  }

  private def verifyFilterInvocations(count: Int) =
    verify(newExpenseFilter, times(count)).withCriterion(fieldCaptor.capture(), operationCaptor.capture(), valueCaptor.capture())

  private def values[T](captor: ArgumentCaptor[T]) = List(captor.getAllValues.toArray: _*)

}
