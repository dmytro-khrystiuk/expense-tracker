package org.expenses.dao

import org.specs2.mutable._
import play.api.db.DB
import play.api.Play.current
import anorm._
import play.api.test.Helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.expenses.ApplicationHelper

@RunWith(classOf[JUnitRunner])
class DBEvolutionsTest extends Specification with ApplicationHelper {

  "Evolutions" should {
    "be applied without errors" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>
          SQL("select count(1) from EXPENSE").execute()
          SQL("select count(1) from USER").execute()
        }
      }
      success
    }
  }

}
