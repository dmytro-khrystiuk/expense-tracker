package org.expenses.dao

import org.specs2.mutable._
import play.api.db.DB
import play.api.Play.current
import play.api.test.Helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.expenses.ApplicationHelper
import org.expenses.models.User

@RunWith(classOf[JUnitRunner])
class SqlUserDaoTest extends Specification with ApplicationHelper with SqlUserUserDaoComponent {
  private val Login = "login"
  private val Name = "name"
  private val Password = "password"

  "UserDao" should {

    "create user" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          userCount must equalTo(0)

          val Some(savedUser) = userDao.create(newUser)

          savedUser.id must equalTo(Some(1L))
          savedUser.login must equalTo(Login)
          savedUser.name must equalTo(Name)
          savedUser.password must equalTo(Password)

          userCount must equalTo(1)
        }
      }
    }

    "return None if user with the same login exists when creating user" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>
          persistUser(newUser)
          userCount must equalTo(1)

          val savedUser = userDao.create(newUser)

          savedUser must equalTo(None)
          userCount must equalTo(1)
        }
      }
    }

    "return User by login if it exists" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>
          persistUser(newUser)

          val Some(user) = userDao.getUserByLogin(Login)

          user must equalTo(User(Some(1L), Login, Name, Password))
        }
      }
    }

    "return None by login if it does not exist" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          val user = userDao.getUserByLogin(Login)

          user must equalTo(None)
        }
      }
    }
  }

  def newUser: User = {
    User(None, Login, Name, Password)
  }

}
