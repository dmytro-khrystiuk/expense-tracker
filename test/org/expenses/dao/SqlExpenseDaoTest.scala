package org.expenses.dao

import org.specs2.mutable._
import play.api.db.DB
import play.api.Play.current
import play.api.test.Helpers._
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.expenses.ApplicationHelper
import org.expenses.models.{Expense, User}
import java.sql.Connection
import java.util.Date
import org.h2.jdbc.JdbcSQLException
import org.expenses.models.filtering.OrderBy
import org.expenses.models.Expense._
import org.expenses.models.filtering.OrderBy._
import org.expenses.models.filtering.Operation._
import scala.util.Failure
import scala.Some
import scala.util.Success

@RunWith(classOf[JUnitRunner])
class SqlExpenseDaoTest extends Specification with ApplicationHelper with SqlExpenseDaoComponent {
  private val UserId: Long = 1L
  private val DayInMillis = 86400000L
  private val ExpenseCount = 10
  private val DefaultOrderBy = OrderBy(Asc, Field.CreationDate)

  private lazy val ExpensesPerDay = (1 to ExpenseCount) map { i =>
      Expense(Some(i), new Date(DayInMillis * i), s"Description$i", BigDecimal(i), UserId)
  }

  "ExpenseDao" should {
    "create expense for user if it exists" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          persistUser(newUser)
          expenseCount must beEqualTo(0)

          val Success(savedExpense) = expenseDao.create(ExpensesPerDay.head)

          savedExpense must beEqualTo(ExpensesPerDay.head)
          expenseCount must beEqualTo(1)

        }
      }
    }

    "update expense if it exists" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          val existentExpense = ExpensesPerDay.head
          val expenseForUpdate = Expense(existentExpense.id, new Date(), "Another Description", BigDecimal(2000), UserId)
          persistUser(newUser)
          persistExpense(existentExpense)
          expenseCount must beEqualTo(1)

          val Success(updatedExpense) = expenseDao.update(expenseForUpdate)

          updatedExpense must beEqualTo(expenseForUpdate)
          expenseCount must beEqualTo(1)

        }
      }
    }

    "return exception when creating or updating expense and user or expense for updating it does not exist" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          expenseCount must beEqualTo(0)

          val Failure(creationException) = expenseDao.create(ExpensesPerDay.head)
          val Failure(updateException) = expenseDao.update(ExpensesPerDay.head)

          expenseCount must beEqualTo(0)
          creationException should beAnInstanceOf[JdbcSQLException]
          updateException should beAnInstanceOf[IllegalArgumentException]

        }
      }
    }

    "return amount and day count when expenses exist" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          persistUser(newUser)
          ExpensesPerDay.foreach(persistExpense)
          expenseCount must beEqualTo(ExpenseCount)

          val (amount, dayCount) = expenseDao.getAmountAndDayCount(newExpenseFilter)

          amount must beEqualTo((1 to ExpenseCount).sum)
          dayCount must beEqualTo(ExpenseCount)

        }
      }
    }

    "return zeroes when expenses do not exist" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          expenseCount must beEqualTo(0)

          val (amount, dayCount) = expenseDao.getAmountAndDayCount(newExpenseFilter)

          amount must beEqualTo(0)
          dayCount must beEqualTo(0)

        }
      }
    }

    "return expected expense list with proper content and order" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          persistUser(newUser)
          ExpensesPerDay.foreach(persistExpense)
          expenseCount must beEqualTo(ExpenseCount)


          actualExpenses() must beEqualTo {
            expectedExpenses()
          }

          actualExpenses(order = OrderBy(Desc, Field.CreationDate)) must beEqualTo {
            expectedExpenses(sorter = _.creationDate.getTime > _.creationDate.getTime)
          }

          actualExpenses(filter = newExpenseFilter.withCriterion(Field.Amount, EQUAL, 1)) must beEqualTo {
            expectedExpenses(predicate = BigDecimal(1) == _.amount)
          }

          val leftBound = new Date(DayInMillis * 3)
          val rightBound = new Date(DayInMillis * 9)

          actualExpenses(filter = newExpenseFilter.withCriterion(Field.CreationDate, GREATER_THAN, leftBound)
                                                  .withCriterion(Field.CreationDate, LESS_THAN, rightBound)
          ) must beEqualTo {
            expectedExpenses(predicate = el =>
              el.creationDate.getTime > leftBound.getTime && el.creationDate.getTime < rightBound.getTime)
          }


        }
      }
    }

    "return empty list when getting expense list and expenses do not exist" in {
      running(fakeApplication) {
        DB.withConnection { implicit connection =>

          expenseCount must beEqualTo(0)

          val expenses = expenseDao.list(newExpenseFilter, DefaultOrderBy)

          expenses must beEmpty
        }
      }
    }

  }

  private def actualExpenses(order: OrderBy = DefaultOrderBy, filter: ExpenseFilter = newExpenseFilter)
                        (implicit connection: Connection) = {
    expenseDao.list(filter, order)
  }

  private def expectedExpenses(predicate: Expense => Boolean = _ => true,
                               sorter: (Expense, Expense) => Boolean = (_, _) => false) = {
    ExpensesPerDay.filter(predicate).sortWith(sorter)
  }

  private def newUser = User(None, "AnyLogin", "AnyName", "AnyPassword")

}
