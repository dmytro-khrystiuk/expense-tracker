package org.expenses.controllers

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._
import org.specs2.execute.Results
import org.expenses.models.User
import play.api.libs.json.Json
import org.expenses.controllers.security.UserInfo.SessionKey
import org.expenses.controllers.security.UserInfo

@RunWith(classOf[JUnitRunner])
class AuthenticationControllerTest extends Specification
                                   with Results
                                   with AuthenticationController
                                   with ControllerTestEnvironment {

  private val Id = 1L
  private val Login = "Login"
  private val Name = "Name"
  private val Password = "Password"
  private val ExpectedUser = User(Some(Id), Login, Name, Password)
  private val UserResponseJsonString: String = s"""{"id":$Id,"login":"$Login","name":"$Name"}"""

  "AuthenticationController" should {
    "return user data when authentication is successful" in {
      running(fakeApplication) {
        // Given
        userService.authenticate(Login, Password) returns Some(ExpectedUser)
        val fakeRequest = FakeRequest().withBody(Json.obj("login" -> Login, "password" -> Password))
        // When
        val response = authenticate.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsString(response) must contain(UserResponseJsonString)
        Helpers.session(response).get(SessionKey.UserId) must beEqualTo(Some(Id.toString))
        Helpers.session(response).get(SessionKey.Name) must beEqualTo(Some(Name))
      }
    }
    "return Bad Request with status 400 when authenticating and invalid JSON received" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest().withBody(Json.obj())
        // When
        val response = authenticate.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(BAD_REQUEST)
      }
    }
    "return Unauthorized with status 401 when authentication failed in service" in {
      running(fakeApplication) {
        // Given
        userService.authenticate(Login, Password) returns None
        val fakeRequest = FakeRequest().withBody(Json.obj("login" -> Login, "password" -> Password))
        // When
        val response = authenticate.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(UNAUTHORIZED)
      }
    }
    "return user data in body and session when registering" in {
      running(fakeApplication) {
        // Given
        userService.register(any[User]) returns Some(ExpectedUser)
        val fakeRequest = FakeRequest().withBody(Json.obj("login" -> Login, "name" -> Name, "password" -> Password))
        // When
        val response = register.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsString(response) must contain(UserResponseJsonString)
        Helpers.session(response).get(SessionKey.UserId) must beEqualTo(Some(Id.toString))
        Helpers.session(response).get(SessionKey.Name) must beEqualTo(Some(Name))
      }
    }
    "return Bad Request with status 400 when registering and invalid JSON received" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest().withBody(Json.obj())
        // When
        val response = register.apply(fakeRequest)

        status(response) must beEqualTo(BAD_REQUEST)
      }
    }
    "return Bad Request with status 400 when registering user service received None" in {
      running(fakeApplication) {
        // Given
        userService.register(any[User]) returns None
        val fakeRequest = FakeRequest().withBody(Json.obj("login" -> Login, "name" -> Name, "password" -> Password))
        // When
        val response = register.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(BAD_REQUEST)
      }
    }
    "return Ok with empty session when logout" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest().withSession(SessionKey.UserId -> Id.toString, SessionKey.Name -> Name)
        // When
        val response = logout.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        Helpers.session(response).isEmpty
      }
    }
    "return Ok with UserInfo when requesting current user and he is stored in session" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest().withSession(SessionKey.UserId -> Id.toString, SessionKey.Name -> Name)
        // When
        val response = currentUser.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsJson(response).as[UserInfo](Json.reads[UserInfo]) must beEqualTo(UserInfo(Id, Name))
      }
    }
    "return Unauthorized with status 401 when requesting current user and he is not stored in session" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest()
        // When
        val response = currentUser.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(UNAUTHORIZED)
      }
    }

  }

}
