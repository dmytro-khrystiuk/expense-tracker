package org.expenses.controllers

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._
import org.specs2.execute.Results
import org.expenses.models.Expense
import org.expenses.controllers.security.UserInfo.SessionKey
import java.util.Date
import play.api.libs.json.{Writes, Json, Reads}
import scala.Some
import scala.util.{Failure, Success}

@RunWith(classOf[JUnitRunner])
class ExpenseControllerTest extends Specification
                            with Results
                            with ExpenseController
                            with ControllerTestEnvironment {

  private val OrderByField = "orderBy"
  private val OrderByValue = "SomeOrder"
  private val Id = 1L
  private val CreationDate = new Date(60000L)
  private val Description = "Description"
  private val Amount = BigDecimal(100)
  private val UserId = 2L
  private val ExpectedExpense = Expense(Some(Id), CreationDate, Description, Amount, UserId)
  private val UrlFilterField = "UrlFilterField"
  private val UrlFilterValue = "UrlFilterValue"

  private implicit val DateReads = Reads.dateReads("yyyy/MM/dd HH:mm")
  private implicit val DateWrites = Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private val ExpenseFormat = Json.format[Expense]

  "ExpenseController" should {
    "return expense by id when it is returned by service" in {
      running(fakeApplication) {
        // Given
        expenseService.getExpense(Id, UserId) returns Some(ExpectedExpense)
        val fakeRequest = authRequest
        // When
        val response = expense(Id).apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsJson(response).as[Expense](ExpenseFormat) must beEqualTo(ExpectedExpense)
      }
    }
    "return Bad Request with status 400 when getting expense and it did not returned by service" in {
      running(fakeApplication) {
        // Given
        expenseService.getExpense(Id, UserId) returns None
        val fakeRequest = authRequest
        // When
        val response = expense(Id).apply(fakeRequest)
        // Then
        status(response) must beEqualTo(BAD_REQUEST)
      }
    }
    "return expenses" in {
      running(fakeApplication) {
        // Given
        expenseService.expensesOfUser(UserId, newExpenseFilter, Some(OrderByValue)) returns List(ExpectedExpense)
        expenseFilterHelper.FilterFields returns Set(UrlFilterField)
        newExpenseFilter.withCriterion(UrlFilterField, UrlFilterValue) answers { (_, mock) => mock.asInstanceOf[ExpenseFilter] }
        val fakeRequest = FakeRequest(Helpers.GET, s"/expenses?$UrlFilterField=UrlFilterValue&$OrderByField=$OrderByValue")
                          .withSession(SessionKey.UserId -> UserId.toString, SessionKey.Name -> "username")
        // When
        val response = expenses.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsJson(response).apply(0).as[Expense](ExpenseFormat) must beEqualTo(ExpectedExpense)
      }
    }
    "return expense data when creating new expense" in {
      running(fakeApplication) {
        // Given
        expenseService.createExpense(any[Expense], any[Long]) returns Success(ExpectedExpense)
        val fakeRequest = authRequest.withBody(Json.toJson(ExpectedExpense)(ExpenseFormat))
        // When
        val response = createExpense.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsJson(response).as[Expense](ExpenseFormat) must beEqualTo(ExpectedExpense)
      }
    }
    "return expense data when updating the expense" in {
      running(fakeApplication) {
        // Given
        expenseService.updateExpense(any[Expense], any[Long]) returns Success(ExpectedExpense)
        val fakeRequest = authRequest.withBody(Json.toJson(ExpectedExpense)(ExpenseFormat))
        // When
        val response = modifyExpense.apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        contentAsJson(response).as[Expense](ExpenseFormat) must beEqualTo(ExpectedExpense)
      }
    }
    "return Bad Request with status 400 when creating or updating expense and service returned IllegalArgumentException" in {
      running(fakeApplication) {
        // Given
        expenseService.createExpense(any[Expense], any[Long]) returns Failure(new IllegalArgumentException)
        expenseService.updateExpense(any[Expense], any[Long]) returns Failure(new IllegalArgumentException)
        val fakeRequest = authRequest.withBody(Json.toJson(ExpectedExpense)(ExpenseFormat))
        // When
        val createResponse = createExpense.apply(fakeRequest)
        val updateResponse = modifyExpense.apply(fakeRequest)
        // Then
        status(createResponse) must beEqualTo(BAD_REQUEST)
        status(updateResponse) must beEqualTo(BAD_REQUEST)
      }
    }
    "return Server Error with status 500 when creating or updating expense and service returned exception with any type except IllegalArgumentException" in {
      running(fakeApplication) {
        // Given
        expenseService.createExpense(any[Expense], any[Long]) returns Failure(new RuntimeException)
        expenseService.updateExpense(any[Expense], any[Long]) returns Failure(new RuntimeException)
        val fakeRequest = authRequest.withBody(Json.toJson(ExpectedExpense)(ExpenseFormat))
        // When
        val createResponse = createExpense.apply(fakeRequest)
        val updateResponse = modifyExpense.apply(fakeRequest)
        // Then
        status(createResponse) must beEqualTo(INTERNAL_SERVER_ERROR)
        status(updateResponse) must beEqualTo(INTERNAL_SERVER_ERROR)
      }
    }
    "return expected report data when getting report for date range" in {
      running(fakeApplication) {
        // Given
        val startDate = new Date(1000L)
        val endDate = new Date(2000L)
        val expectedTotal = BigDecimal(100)
        val expectedAverage = BigDecimal(200)
        expenseService.reportForDateRange(UserId, startDate, endDate) returns {
          (List(ExpectedExpense), expectedTotal, expectedAverage)
        }
        val fakeRequest = authRequest
        // When
        val response = reportForRange(startDate, endDate).apply(fakeRequest)
        // Then
        status(response) must beEqualTo(OK)
        val responseJson = contentAsJson(response)
        (responseJson \ "expenses").apply(0).as[Expense](ExpenseFormat) must beEqualTo(ExpectedExpense)
        (responseJson \ "total").as[BigDecimal] must beEqualTo(expectedTotal)
        (responseJson \ "average").as[BigDecimal] must beEqualTo(expectedAverage)
      }
    }
    "return Unauthorized response with status 401 for every method if there is no user in session" in {
      running(fakeApplication) {
        // Given
        val fakeRequest = FakeRequest()
        // When
        val responses = Set(
          expense(1L).apply(fakeRequest),
          expenses.apply(fakeRequest),
          createExpense.apply(fakeRequest.withBody(Json.obj())),
          modifyExpense.apply(fakeRequest.withBody(Json.obj())),
          reportForRange(new Date(), new Date()).apply(fakeRequest)
        ).map(status)
        // Then
        responses.size must beEqualTo(1)
        responses.head must beEqualTo(UNAUTHORIZED)
      }
    }

  }


  private def authRequest = FakeRequest().withSession(SessionKey.UserId -> UserId.toString, SessionKey.Name -> "username")

}
