package org.expenses.controllers.security

import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import org.scalatest.{BeforeAndAfter, FunSuite}
import play.api.mvc.{Session, RequestHeader}
import org.mockito.Mockito._
import UserInfo._

@RunWith(classOf[JUnitRunner])
class UserInfoTest extends FunSuite with BeforeAndAfter {
  private val UserId = 1L
  private val Username = "SomeUsername"

  private val Request: RequestHeader = mock(classOf[RequestHeader])
  private val Session: Session = mock(classOf[Session])

  before {
    reset(Request, Session)
    when(Request.session).thenReturn(Session)
  }

  test("fromRequest should return some UserInfo from request when it exists") {
    // Given
    when(Session.get(SessionKey.UserId)).thenReturn(Some(UserId.toString))
    when(Session.get(SessionKey.Name)).thenReturn(Some(Username))
    // When
    val Some(userInfo) = UserInfo.fromRequest(Request)
    // Then
    assert(UserId === userInfo.id)
    assert(Username === userInfo.name)
  }

  test("fromRequest should return None when id is absent in session") {
    // Given
    when(Session.get(SessionKey.UserId)).thenReturn(None)
    when(Session.get(SessionKey.Name)).thenReturn(Some(Username))
    // When
    val userInfo = UserInfo.fromRequest(Request)
    // Then
    assert(None === userInfo)
  }

  test("fromRequest should return None when name is absent in session") {
    // Given
    when(Session.get(SessionKey.UserId)).thenReturn(Some(UserId.toString))
    when(Session.get(SessionKey.Name)).thenReturn(None)
    // When
    val userInfo = UserInfo.fromRequest(Request)
    // Then
    assert(None === userInfo)
  }

  test("fromRequest should throw exception when id in session is not in number format") {
    // Given
    when(Session.get(SessionKey.UserId)).thenReturn(Some("NotNumber"))
    when(Session.get(SessionKey.Name)).thenReturn(Some(Username))
    // When Then
    intercept[NumberFormatException] {
      UserInfo.fromRequest(Request)
    }
  }

}
