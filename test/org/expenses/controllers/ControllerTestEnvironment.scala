package org.expenses.controllers

import org.expenses.services.{ExpenseServiceComponent, UserServiceComponent}
import org.expenses.dao.{UserDaoComponent, ExpenseDaoComponent}
import org.expenses.controllers.security.DefaultSecurityHelperComponent
import org.expenses.models.filtering.ExpenseFilterComponent
import org.specs2.mock.Mockito
import org.expenses.ApplicationHelper

trait ControllerTestEnvironment extends UserServiceComponent
                                with UserDaoComponent
                                with ExpenseServiceComponent
                                with ExpenseDaoComponent
                                with ExpenseFilterComponent
                                with DefaultSecurityHelperComponent
                                with Mockito
                                with ApplicationHelper {

  override val userService: UserService = mock[UserService]
  override val expenseService: ExpenseService = mock[ExpenseService]

  override val expenseDao: ExpenseDao = mock[ExpenseDao]
  override val userDao: UserDao = mock[UserDao]

  override val newExpenseFilter: ExpenseFilter = mock[ExpenseFilter]
  override val expenseFilterHelper: ExpenseFilterHelper = mock[ExpenseFilterHelper]
}

